package com.crsystem.springjwt.repository;

import com.crsystem.springjwt.models.Municipality;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface MunicipalityRepository  extends JpaRepository<Municipality, Long>,
       QuerydslPredicateExecutor<Municipality> {

    Municipality findById(long id);
}

