package com.crsystem.springjwt.repository;

import com.crsystem.springjwt.models.District;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface DistrictRepository extends JpaRepository<District, Long>,
        QuerydslPredicateExecutor<District> {

    District findById(long id);

    District findByProvince(long id);


}

