package com.crsystem.springjwt.repository;

import com.crsystem.springjwt.models.District;
import com.crsystem.springjwt.models.Province;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface ProvinceRepository extends JpaRepository<Province, Long>,
        QuerydslPredicateExecutor<Province> {

    Province findById(long id);
}
