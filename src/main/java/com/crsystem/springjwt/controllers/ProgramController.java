package com.crsystem.springjwt.controllers;

import com.crsystem.springjwt.models.Program;
import com.crsystem.springjwt.security.services.ProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/test/program")
@CrossOrigin(origins = {"http://localhost:4200"})
public class ProgramController {

    @Autowired
    private ProgramService service;

    @GetMapping
    public Page<Program> getAll(@PageableDefault(size=5, sort="id") final Pageable page,
                                @RequestParam(required=false) final String name,
                                @RequestParam(required=false) final String department,
                                @RequestParam(required=false) final String funds,
                                @RequestParam(required=false) final String target) {
        return service.getAll(page, name, department, funds,target);
    }

    @GetMapping(value = "/all")
    public List<Program> getAll() {
        return service.getAll();
    }

    @PostMapping
    public Program load(@RequestBody final Program program) {
        return service.create(program);
    }

    @GetMapping(value = "/{id}")
    public Program findById(@PathVariable final long id) {
        return service.findById(id);
    }

    @PutMapping(value = "/{id}")
    public Program updateRep(@RequestBody final Program program, @PathVariable final long id) {
        return service.update(program, id);
    }

    @DeleteMapping(value = "/{id}")
    public Program deleteRep(@PathVariable final long id) {
        return service.deleteById(id);
    }
}
