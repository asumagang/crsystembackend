package com.crsystem.springjwt.controllers;

import com.crsystem.springjwt.models.District;
import com.crsystem.springjwt.models.Province;
import com.crsystem.springjwt.security.services.DistrictService;
import com.crsystem.springjwt.security.services.ProvinceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/test/province")
@CrossOrigin(origins = {"http://localhost:4200"})
public class ProvinceController {

    @Autowired
    private ProvinceService provinceService;

    @GetMapping(value = "/all")
    public List<Province> getAll() {
        return provinceService.getAll();
    }

    @PostMapping
    public Province load(@RequestBody final Province province) {
        return provinceService.create(province);
    }

    @GetMapping(value = "/{id}")
    public Province findById(@PathVariable final long id) {
        return provinceService.findById(id);
    }

    @PutMapping(value = "/{id}")
    public Province updateProvince(@RequestBody final Province province, @PathVariable final long id) {
        return provinceService.update(province, id);
    }

    @DeleteMapping(value = "/{id}")
    public Province deleteProvince(@PathVariable final long id) {
        return provinceService.deleteById(id);
    }
}
