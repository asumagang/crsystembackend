package com.crsystem.springjwt.controllers;


import com.crsystem.springjwt.models.Program;
import com.crsystem.springjwt.models.User;
import com.crsystem.springjwt.security.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/test/users")
@CrossOrigin(origins = {"http://localhost:4200"})
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/all")
    public List<User> getAll() {
        return userService.getAll();
    }


    @GetMapping(value = "/{id}")
    public User findById(@PathVariable final long id) {
        return userService.findById(id);
    }

    @PostMapping
    public User load(@RequestBody final User user) {
        return userService.create(user);
    }

    @PutMapping(value = "/{id}")
    public User updateUser(@RequestBody final User user, @PathVariable final long id) {
        return userService.update(user, id);
    }

    @DeleteMapping(value = "/{id}")
    public User deleteUser(@PathVariable final long id) {
        return userService.deleteById(id);
    }

}
