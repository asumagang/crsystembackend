package com.crsystem.springjwt.controllers;

import com.crsystem.springjwt.models.District;
import com.crsystem.springjwt.repository.DistrictRepository;
import com.crsystem.springjwt.security.services.DistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/test/districts")
@CrossOrigin(origins = {"http://localhost:4200"})
public class DistrictController {

    @Autowired
    private DistrictService districtService;
    @Autowired
    private DistrictRepository districtRepository;

//    @GetMapping
//    public Page<User> getAll(@PageableDefault(size=5, sort="id") final Pageable page,
//                             @RequestParam(required=false) final String firstName,
//                             @RequestParam(required=false) final String lastName,
//                             @RequestParam(required=false) final String occupation) {
//        return service.getAll(page, firstName, lastName, occupation);
//    }

    @GetMapping(value = "/all")
    public List<District> getAll() {
        return districtService.getAll();
    }

    @PostMapping
    public District load(@RequestBody final District district) {
        return districtService.create(district);
    }

    @GetMapping(value = "/{id}")
    public District findById(@PathVariable final long id) {
        return districtService.findById(id);
    }

    @PutMapping(value = "/{id}")
    public District updateDistrict(@RequestBody final District district, @PathVariable final long id) {
        return districtService.update(district, id);
    }

    @DeleteMapping(value = "/{id}")
    public District deleteDistrict(@PathVariable final long id) {
        return districtService.deleteById(id);
    }

    @GetMapping(value = "/province/{id}")
    public District findByProvinceId(@PathVariable final long id){
        return districtService.findByProvince(id);
    }


}
