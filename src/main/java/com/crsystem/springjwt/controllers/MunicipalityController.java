package com.crsystem.springjwt.controllers;

import com.crsystem.springjwt.models.District;
import com.crsystem.springjwt.models.Municipality;
import com.crsystem.springjwt.security.services.MunicipalityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/test/municipalities")
@CrossOrigin(origins = {"http://localhost:4200"})
public class MunicipalityController {

    @Autowired
    private MunicipalityService municipalityService;

//    @GetMapping
//    public Page<Todo> getAll(@PageableDefault(size=5, sort="id") final Pageable page,
//                             @RequestParam(required=false) final String ownerName,
//                             @RequestParam(required=false) final String name,
//                             @RequestParam(required=false) final String status
//    ){
//        return service.getAll(page,ownerName,name,status);
//    }
@GetMapping(value = "/all")
public List<Municipality> getAll() {
    return municipalityService.getAll();
}


    @PostMapping
    public Municipality load(@RequestBody final Municipality municipality) {
        return municipalityService.create(municipality);
    }

    @GetMapping(value = "/{id}")
    public Municipality findById(@PathVariable final long id) {
        return municipalityService.findById(id);
    }

    @PutMapping(value = "/{id}")
    public Municipality updateMunicipality(@RequestBody final Municipality municipality, @PathVariable final long id) {
        municipality.setId(id);
        return municipalityService.update(municipality);
    }

    @DeleteMapping(value = "/{id}")
    public Municipality deleteMunicipality(@PathVariable final long id) {
        return municipalityService.deleteById(id);
    }
}
