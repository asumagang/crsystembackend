package com.crsystem.springjwt.models;

public enum ERole {
	ROLE_USER,
    ROLE_PDPO,
    ROLE_ADMIN
}
