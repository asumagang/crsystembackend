package com.crsystem.springjwt.security.services;

import com.crsystem.springjwt.models.District;
import com.crsystem.springjwt.models.Province;
import com.crsystem.springjwt.repository.DistrictRepository;
import com.crsystem.springjwt.repository.ProvinceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProvinceService {

    @Autowired
    private ProvinceRepository provinceRepository;

    // GET
    public List<Province> getAll() {
        return provinceRepository.findAll();
    }

    // GET
    public Province findById(long id) {
        Province province = provinceRepository.findById(id);
        if(province == null) {
            throw new RuntimeException("Province not found");
        }
        return province;
    }

    // POST
    public Province create(Province province) {
        return provinceRepository.save(province);
    }

    // PUT
    public Province update(Province province, long id) {
        province.setId(id);
        return provinceRepository.save(province);
    }

    // DELETE
    public Province deleteById(long id) {
        Province province = findById(id);
        if(province == null) {
            throw new RuntimeException("Province does not exist");
        }
        provinceRepository.deleteById(id);
        return province;
    }

}
