package com.crsystem.springjwt.security.services;

import com.crsystem.springjwt.models.District;
import com.crsystem.springjwt.models.Municipality;
import com.crsystem.springjwt.models.Province;
import com.crsystem.springjwt.repository.DistrictRepository;
import com.crsystem.springjwt.repository.MunicipalityRepository;
import com.querydsl.core.BooleanBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MunicipalityService {

    @Autowired
    private MunicipalityRepository municipalityRepository;

    @Autowired
    private DistrictService districtService;

    @Autowired
    private ProvinceService provinceService;

    // GET
    public List<Municipality> getAll() {
        return municipalityRepository.findAll();
    }

    // GET
    public Municipality findById(long id) {
        Municipality municipality = municipalityRepository.findById(id);
        if(municipality == null) {
            throw new RuntimeException("Municipality not found");
        }
        return municipality;
    }

    // POST
    public Municipality create(Municipality municipality) {

        District muniDistrict = districtService.findById(municipality.getDistrict().getId());
        Province muniProvince = provinceService.findById(municipality.getProvince().getId());
        municipality.setDistrict(muniDistrict);
        municipality.setProvince(muniProvince);
        return municipalityRepository.save(municipality);
    }

    // PUT
    public Municipality update(Municipality municipality) {
        return municipalityRepository.save(municipality);
    }

    // DELETE
    public Municipality deleteById(long id) {
        Municipality municipality = findById(id);
        if(municipality == null) {
            throw new RuntimeException("Municipality does not exist");
        }
        municipalityRepository.deleteById(id);
        return municipality;
    }
}
