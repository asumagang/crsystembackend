package com.crsystem.springjwt.security.services;

import com.crsystem.springjwt.models.District;
import com.crsystem.springjwt.models.Municipality;
import com.crsystem.springjwt.models.Province;
import com.crsystem.springjwt.repository.DistrictRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DistrictService {

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private ProvinceService provinceService;


    Province province;

    // GET
    public List<District> getAll() {
        return districtRepository.findAll();
    }

    // GET
    public District findById(long id) {
        District district = districtRepository.findById(id);
        if(district == null) {
            throw new RuntimeException("District not found");
        }
        return district;
    }

    // POST
    public District create(District district) {

        Province districtProvince = provinceService.findById(district.getProvince().getId());
        district.setProvince(districtProvince);
        return districtRepository.save(district);
    }

    // PUT
    public District update(District district, long id) {
        district.setId(id);
        return districtRepository.save(district);
    }

    // DELETE
    public District deleteById(long id) {
        District district = findById(id);
        if(district == null) {
            throw new RuntimeException("District does not exist");
        }
        districtRepository.deleteById(id);
        return district;
    }

    public District findByProvince(long id) {
        District district = findByProvince(id);
        if(district == null) {
            throw new RuntimeException("District does not exist");
        }
        return district;
    }
}
