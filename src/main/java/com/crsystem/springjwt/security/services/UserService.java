package com.crsystem.springjwt.security.services;

import com.crsystem.springjwt.models.Program;
import com.crsystem.springjwt.models.User;
import com.crsystem.springjwt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    // GET ALL
    public List<User> getAll() {
        return userRepository.findAll();
    }

    // GET
    public User findById(long id) {
        User user = userRepository.findById(id);
        if(user == null) {
            throw new RuntimeException("User not found");
        }
        return user;
    }
    //POST
    public User create(User user) {
        return userRepository.save(user);
    }

    // PUT
    public User update(User user, long id) {
        user.setId(id);
        return userRepository.save(user);
    }

    // DELETE
    public User deleteById(long id) {
        User user = findById(id);
        if(user == null) {
            throw new RuntimeException("User does not exist");
        }
        userRepository.deleteById(id);
        return user;
    }
}
